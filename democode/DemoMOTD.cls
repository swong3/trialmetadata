public class DemoMOTD {
    
    static public String getMOTD() {
        Http httpProtocol = new Http();
        HttpRequest request = new HttpRequest();
        String endpoint = 'https://type.fit/api/quotes';
        request.setEndpoint(endpoint);
        request.setMethod('GET');
        HttpResponse response = httpProtocol.send(request);
        System.debug(response.getBody());
        
        JSONParser parser = JSON.createParser(response.getBody());
        String[] messages = new String[]{};
        String[] authors = new String[]{};
        
        while (parser.nextToken() != null) {
            JSONToken token = parser.getCurrentToken();
            if ((token==JSONToken.FIELD_NAME) && 
                	(parser.getText()=='text')) {
            	parser.nextToken();
                messages.add(parser.getText());
            }
            else if ((token==JSONToken.FIELD_NAME) && 
                     (parser.getText()=='author')) {
				parser.nextToken();
                authors.add(parser.getText());
            }
        }
        Integer noOfMessages = messages.size();
        Integer randomNo = Integer.valueOf(Math.random() * noOfMessages);
		String motd = messages[randomNo] + ' - ' + authors[randomNo];   
        
        return motd;
    }

}
